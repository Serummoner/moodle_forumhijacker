<?php
// This file is part of eMailTest plugin for Moodle - http://moodle.org/
//
// eMailTest is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// eMailTest is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with eMailTest.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Index page for local_forumhijacker.
 *
 * @package    local_forumhijacker
 * @copyright  2019 vhb (Virtuelle Hochschule Bayern) - www.vhb.org
 * @author     Sebastian Boosz
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

//moodleform is defined in formslib.php
require_once("$CFG->libdir/formslib.php");

class local_forumhijacker_action_form extends moodleform {

    public function definition() {
        global $CFG;

        $victim_id = $this->_customdata['victim_id'];

        $mform = $this->_form;

        $mform->addElement('hidden', 'victim_id', $victim_id);
        $mform->setType('victim_id', PARAM_INT);

        $this->add_action_buttons(false, 'Hijack!');

    }
}