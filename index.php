<?php
// This file is part of eMailTest plugin for Moodle - http://moodle.org/
//
// eMailTest is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// eMailTest is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with eMailTest.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Index page for local_forumhijacker.
 *
 * @package    local_forumhijacker
 * @copyright  2019 vhb (Virtuelle Hochschule Bayern) - www.vhb.org
 * @author     Sebastian Boosz
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use local_forumhijacker\output\index_page;

require_once(__DIR__.'/../../config.php');
require_once($CFG->dirroot . '/local/forumhijacker/lib.php');
require_once($CFG->dirroot . '/local/forumhijacker/action_form.php');
require_once($CFG->libdir.'/adminlib.php');

$pluginname = 'forumhijacker';

// Globals.
global $CFG, $OUTPUT, $USER, $SITE, $PAGE, $DB;

check_admin();

admin_externalpage_setup('local_'.$pluginname); // Sets the navbar & expands navmenu.

$title = get_local_string('pluginname', 'local_'.$pluginname);
$heading = get_local_string('heading', 'local_'.$pluginname);
$url = new moodle_url('/local/' . $pluginname . '/');
$context = context_system::instance();

$PAGE->set_pagelayout('admin');
$PAGE->set_url($url);
$PAGE->set_context($context);
$PAGE->set_title($title);
$PAGE->set_heading($heading);

$output = $PAGE->get_renderer('local_forumhijacker');

echo $output->header();
echo $output->heading($heading);

// check that the configured hijacker user account is a deleted account
$hijacker_id = $CFG->hijacker_id;

$hijacker_ok = check_valid_hijacker($hijacker_id);
if (!$hijacker_ok) {
    return;
}

// If we get here via POST request, try to process POST data. At this point we have already checked for ADMIN access and
// made sure that the configured hijacker id is okay.
$action_form = new local_forumhijacker_action_form();
if ($fromform = $action_form->get_data()) {

    $db_table = 'tool_dataprivacy_request';
    
    $victim_id = $fromform->victim_id;
    
    // check that the victim id is okay (has open GDPR deletion request)
    if (check_hijack_victim($victim_id)) {
        // Forenposts (und angehängte Dateien) umbiegen
        $discussions_table = 'forum_discussions';

        // mdl_forum_discussions on (where userid == victim_id)
        $discussions = get_user_discussions($victim_id);
        // update each discussion
        foreach ($discussions as $index => $discussion) {
            $discussion->userid = $hijacker_id;
            $DB->update_record($discussions_table, $discussion);
        }

        // mdl_forum_discussions on (where usermodified == victim_id)
        $discussions_modified = get_usermodified_discussions($victim_id);
        // update each discussion
        foreach ($discussions_modified as $index => $discussion_modified) {
            $discussion_modified->usermodified = $hijacker_id;
            $DB->update_record($discussions_table, $discussion);
        }
        
        // mdl_forum_posts on (where userid == victim_id)
        $posts = get_user_posts($victim_id);
        $posts_table = 'forum_posts';
        // update each post
        foreach ($posts as $index => $post) {
            $post->userid = $hijacker_id;
            $DB->update_record($posts_table, $post);
        }

        // attachments - not necessary?
        // mdl_files on (userid == victim_id, component == mod_forum, filearea == post || filearea == attachment)


        // let's assume everything is working
    } else {
        // print_r("The user with id " . $victim_id . " must not be hijacked!!!");
        redirect($url, 'That user cannot be hijacked!', null, \core\output\notification::NOTIFY_ERROR);
    }

    ?>
    <a href="<?php echo $url; ?>">Zurück</a>
    <?php
    exit();
}


// Get people with active GDPR deletion requests
$db_table = 'tool_dataprivacy_request';
$result = $DB->get_records_select($db_table, "type = 2 AND (status = 2 OR status = 3) AND userid != " . $CFG->hijacker_id, array(), '', "id,type,userid,requestedby,status,timecreated");

$table_rows = array();
foreach ($result as $id => $fields) {
    $user_id = $fields->userid;
    $time_created = $fields->timecreated;
    $db_user = $DB->get_record("user", array('id' => $user_id));

    $first_name = $db_user->firstname;
    $last_name = $db_user->lastname;

    $num_user_discussions = $DB->count_records('forum_discussions', array('userid' => $user_id)); 
    $num_user_posts = $DB->count_records('forum_posts', array('userid' => $user_id)); 

    $user_obj = new stdClass();
    $user_obj->user_id = $user_id;
    $user_obj->time_created = $time_created;
    $user_obj->first_name = $first_name;
    $user_obj->last_name = $last_name;
    $user_obj->num_user_discussions = $num_user_discussions;
    $user_obj->num_user_posts = $num_user_posts;

    $action_form = new local_forumhijacker_action_form(null, array("victim_id" => $user_id));
    $user_obj->action_form = $action_form->render();

    $table_rows[] = $user_obj;
}

$renderable = new index_page($table_rows);
echo $output->render($renderable);


echo $output->footer();
