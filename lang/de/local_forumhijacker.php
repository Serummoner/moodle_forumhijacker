<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     local_forumhijacker
 * @category    string
 * @copyright   2019 Sebastian Boosz <sebastian.boosz@vhb.org>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Forum Post Hijacker';
$string['privacy:metadata'] = "Dieses Plug-In speichert keine personenbezogenen Daten.";
$string['welcome'] = 'Hallo Welt!';
$string['heading'] = 'Forenpostbesitz umbiegen';

// Table headings
$string['userid'] = 'Nutzer-ID';
$string['firstname'] = 'Vorname';
$string['lastname'] = 'Nachname';
$string['request_date'] = 'Datum der Löschanfrage';
$string['num_discussions'] = 'Gestartete Diskussionen';
$string['num_posts'] = 'Forenbeiträge';
$string['actions'] = 'Aktionen';

// settings page
$string['hijacker_settings'] = 'Forum Hijacker Einstellungen';
$string['hijacker_id'] = 'Hijacker-ID';
$string['hijacker_id_help'] = 'Nutzer-ID des Accounts, dem Forenposts zugeschrieben werden sollen.';
$string['hijacker_info'] = '<h3>Konfigurierter Hijacker-Account</h3><dl><dt>Nutzer-ID</dt><dd>{$a->hijackerid}</dd><dt>Vor- und Nachname</dt><dd>{$a->hijackerfirstname} {$a->hijackerlastname}</dd><dt>Nutzername (Loginname)</dt><dd>{$a->hijackerusername}</dd></dl>';

// errors
$string['hijacker_error_no_victims'] = 'Aktuell kommen keine Nutzer/innen für einen Forenpost-Hijack in Frage. Infrage kommen nur Nutzerkonten, für die eine aktive DSGVO-Löschenanfrage existiert.';
$string['hijacker_not_existing'] = '<p>Es gibt zur konfigurierten  Hijacker-ID keinen Nutzeraccount! Die Ausführung des Plugins wird aus Sicherheitsgründen abgebrochen!</p>
                                    <p>Konfigurieren Sie in den <a href="{$a}">Forum Hijacker Einstellungen</a> die ID eines gelöschten Nutzeraccounts!</p>';
$string['hijacker_not_deleted'] =  '<p>Beim konfigurierten Hijacker-Konto handelt es nicht um einen gelöschten Nutzer! Die Ausführung des Plugins wird aus Sicherheitsgründen abgebrochen!</p>
                                    <p>Konfigurieren Sie in den <a href="{$a}">Forum Hijacker Einstellungen</a> die ID eines gelöschten Nutzeraccounts oder löschen Sie den konfigurierten Account!</p>';