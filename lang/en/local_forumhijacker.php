<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     local_forumhijacker
 * @category    string
 * @copyright   2019 Sebastian Boosz <sebastian.boosz@vhb.org>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Forum Post Hijacker';
$string['privacy:metadata'] = 'This plugin does not store any user data.';
$string['welcome'] = 'Hello World!';
$string['heading'] = 'Transfer Forum Post Ownership';

// Table headings
$string['userid'] = 'User ID';
$string['firstname'] = 'First name';
$string['lastname'] = 'Last name';
$string['request_date'] = 'Date of deletion request';
$string['num_discussions'] = 'Started discussions';
$string['num_posts'] = 'Forum posts';
$string['actions'] = 'Actions';

// settings page
$string['hijacker_settings'] = 'Forum Hijacker Settings';
$string['hijacker_id'] = 'Hijacker ID';
$string['hijacker_id_help'] = 'User ID of the account the forum posts and discussions should be assigned to.';
$string['hijacker_info'] = '<h3>Configured Hijacker account</h3><dl><dt>User ID</dt><dd>{$a->hijackerid}</dd><dt>First and last name</dt><dd>{$a->hijackerfirstname} {$a->hijackerlastname}</dd><dt>User name</dt><dd>{$a->hijackerusername}</dd></dl>';

// errors
$string['hijacker_error_no_victims'] = 'Currently there are no users whose forum posts can be hijacked. Hijacking is only possible for user accounts with an active GDPR deletion request.';
$string['hijacker_not_existing'] = '<p>There is no account belonging to the configured hijacker_id! Execution is aborted for security reasons!</p>
                                    <p>You have to configure a deleted user account in the<a href="{$a}">Forum Hijacker settings</a> before you can proceed</p>';
$string['hijacker_not_deleted'] =  '<p>The account belonging to the configured hijacker_id is not deleted! Execution is aborted for security reasons!</p>
                                    <p>You have to configure a deleted user account in the<a href="{$a}">Forum Hijacker settings</a> before you can proceed</p>';
