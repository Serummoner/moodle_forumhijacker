<?php
// This file is part of eMailTest plugin for Moodle - http://moodle.org/
//
// eMailTest is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// eMailTest is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with eMailTest.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Index page for local_forumhijacker.
 *
 * @package    local_forumhijacker
 * @copyright  2019 vhb (Virtuelle Hochschule Bayern) - www.vhb.org
 * @author     Sebastian Boosz
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

// utility
function get_local_string($key) {
    global $pluginname;
    return get_string($key, 'local_' . $pluginname);
}

function check_valid_hijacker($hijacker_id) {
    global $DB;
    global $pluginname;

    $hijacker_user = $DB->get_record("user", array('id' => $hijacker_id));
    if (!$hijacker_user) {
        $setting_url = new moodle_url('/admin/settings.php?section=forumhijacker');
        echo get_string('hijacker_not_existing', 'local_' . $pluginname, strval($setting_url));
        return false;
    }
    $deleted = $hijacker_user->deleted;
    if (!$deleted) {
        $setting_url = new moodle_url('/admin/settings.php?section=forumhijacker');
        echo get_string('hijacker_not_deleted', 'local_' . $pluginname, strval($setting_url));
        return false;
    } else {

        $a = new stdClass();
        $a->hijackerid = $hijacker_user->id;
        $a->hijackerfirstname = $hijacker_user->firstname;
        $a->hijackerlastname = $hijacker_user->lastname;
        $a->hijackerusername = $hijacker_user->username;

        echo get_string('hijacker_info', 'local_' . $pluginname, $a);
        return true;
    }
}

function check_hijack_victim($hijack_victim_id) {
    global $DB;

    $db_table = 'tool_dataprivacy_request';

    return $DB->count_records($db_table, ['userid' => strval($hijack_victim_id), "type" => 2, "status" => 2, "status" => 3]);
}

function check_admin() {
    // Ensure only administrators have access.
    $homeurl = new moodle_url('/');
    require_login();
    if (!is_siteadmin()) {
        redirect($homeurl, "This feature is only available for site administrators.", 5);
    }
}

/*
$num_user_discussions = $DB->count_records('forum_discussions', array('userid' => $user_id)); 
$num_user_posts = $DB->count_records('forum_posts', array('userid' => $user_id)); 
*/

function get_user_discussions($user_id) {
    global $DB;
    $discussions_table = 'forum_discussions';
    return $DB->get_records($discussions_table, array('userid' => $user_id));
}

function get_usermodified_discussions($user_id){
    global $DB;
    $discussions_table = 'forum_discussions';
    return $DB->get_records($discussions_table, array('usermodified' => $user_id));
}

function get_user_posts($user_id) {
    global $DB;
    $posts_table = 'forum_posts';
    return $DB->get_records($posts_table, array('userid' => $user_id));
}