<?php
// This file is part of eMailTest plugin for Moodle - http://moodle.org/
//
// eMailTest is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// eMailTest is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with eMailTest.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Index Page implementation for local_forumhijacker.
 *
 * @package    local_forumhijacker
 * @copyright  2019 vhb (Virtuelle Hochschule Bayern) - www.vhb.org
 * @author     Sebastian Boosz
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace local_forumhijacker\output;

use renderable;
use renderer_base;
use templatable;
use stdClass;

class index_page implements renderable, templatable {
    /** @var array $table_data The table rows data which is passed to the template. */
    var $table_data = null;

    public function __construct($table_data) {
        $this->table_data = $table_data;
    }

    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @return stdClass
     */
    public function export_for_template(renderer_base $output) {
        $data = new stdClass();
        $data->table_data = $this->table_data;

        $has_rows = false;
        if(!empty($this->table_data)){
            $has_rows = true;
        }

        $data->has_rows = $has_rows;
        return $data;
    }
}