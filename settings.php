<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin administration pages are defined here.
 *
 * @package     local_forumhijacker
 * @category    admin
 * @copyright   2019 Sebastian Boosz <sebastian.boosz@vhb.org>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig) {
    $pluginid = "forumhijacker";
    $pluginname = get_string('pluginname', 'local_forumhijacker');

    $ADMIN->add('server', new admin_externalpage('local_forumhijacker',
        $pluginname,
        new moodle_url('/local/forumhijacker/index.php')));


    $settings = new admin_settingpage('forumhijacker', get_string('hijacker_settings', 'local_' . $pluginid));

    $settings->add(new admin_setting_configtext('hijacker_id',
        get_string('hijacker_id','local_' . $pluginid), get_string('hijacker_id_help', 'local_' . $pluginid), ''));

    $ADMIN->add('localplugins', $settings);

}
